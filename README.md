# Add #vBIB Metadata to Wikidata

Parse metadata from the website of the German virtual library conference #vBIB and add it to Wikidata

* **#vbib20:** https://events.tib.eu/vbib20
    * [Parser](vBIB20_programm.ipynb)
    * [JSON-Export](vBIB20_metadata.json)
    * [OpenRefine Project](vBIB20.openrefine.tar.gz)


* **#vbib21:** https://www.vbib.net/vbib21-programm/
    * [Parser](vBIB21_programm.ipynb)
    * [JSON-Export](vBIB21_metadata.json)
    * [OpenRefine Schema](vBIB21_schema.json)

* **#vbib22:** https://www.vbib.net/vbib22-programm/
    * [Parser](vBIB22_programm.ipynb)
    * [JSON-Export](vBIB22_metadata.json)
    * [OpenRefine Schema](vBIB22_schema.json)
    
    
## OpenRefine Processing

* Create two projects "vBIBYY_Programm", "vBIBYY_Speaker"
* For better reconciliation of the speaker, fetch the orcid from the speaker project into the programme project with the `cell.cross()` and strip the orcid-domain: `cell.cross("vBIB23_Speaker","speaker")[0].cells["orcid"].value.replace("https://orcid.org/","")`
* Reconcile the title column as base column for the wikidata item
* Reconcile the speaker column with help of the added orcid column